package com.paytm.bank.ts.debeziumdemo;

import com.commons.utility.CommonsUtility;
import com.paytm.bank.ts.debeziumdemo.utility.CommonUtils;
import io.debezium.config.Configuration;
import io.debezium.embedded.EmbeddedEngine;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.connect.source.SourceRecord;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class DebeziumService {

  @PostConstruct
  public void init() {
    Configuration configuration = Configuration.create()
        .with("connector.class","io.debezium.connector.mysql.MySqlConnector")
        .with("name", "mysql-connector")
        .with("database.hostname", "localhost")
        .with("database.port", 3306)
        .with("database.user", "root")
        .with("database.password", "root")
        .with("database.whitelist", "transaction")
        .with("table.whitelist", "transaction.fis_transactions")
        //.with("database.server.id", "1234")
        .with("database.server.name", "bank")
        //.with("database.history.kafka.bootstrap.servers", "localhost:9092")
        //.with("database.history.kafka.topic", "dbhistory.transaction")
        .with("include.schema.changes", true)

        .with("database.history",
            "io.debezium.relational.history.KafkaDatabaseHistory")
        /*.with("database.history.file.filename",
            "work/debezium/dbhistory.dat")*/
        .with("database.history.kafka.topic","transaction.dbhistoryv4")
        .with("database.history.kafka.partitions", 2)
        .with("database.history.kafka.replication.factor",(short)2)
        .with("database.history.kafka.bootstrap.servers", "localhost:9092")
        .with("offset.storage",
            "org.apache.kafka.connect.storage.KafkaOffsetBackingStore")
        .with("offset.storage.topic","transaction.fis_transactionsv4")
        .with("offset.storage.partitions", 2)
        .with("offset.storage.replication.factor",(short)2)
        .with("bootstrap.servers", "localhost:9092")
        .with("internal.key.converter", "org.apache.kafka.connect.json.JsonConverter")
        .with("internal.value.converter", "org.apache.kafka.connect.json.JsonConverter")
        .build();

    EmbeddedEngine engine = EmbeddedEngine.create()
        .using(configuration)
        .notifying(this::handleEvent)
        .build();

    Executor executor = new ThreadPoolExecutor(1,1,60, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));
    executor.execute(engine);
  }

  public void handleEvent(SourceRecord sourceRecord) {
    //sourceRecord
    try {
      int key = (int) Math.random() * 10;
      int temp = 123;
    } catch (Exception e) {
      log.error("Exception while producing record {}", CommonsUtility.exceptionFormatter(e));
    }
  }

}
