package com.paytm.bank.ts.debeziumdemo.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class CommonUtils {

  private static ObjectMapper objectMapper = new ObjectMapper();

  public static <U> U jsonStringToObject(String jsonString, Class<U> clazz) throws IOException {
    return objectMapper.readValue(jsonString, clazz);
  }

  public static String jsonObjectToString(Object object) throws JsonProcessingException {
    return objectMapper.writeValueAsString(object);
  }
}
